from typing import List, Optional
from matplotlib import colors
import matplotlib.pyplot as plt


def display_nonogram(nonogram_matrix: List[List[int]], output_file_path: Optional[str] = None):
    cmap = colors.ListedColormap(["grey", "white", "black"])
    bounds = [-2, -0.5, 0.5, 1.5]  # intervals that will map -1 to grey, 0 to white and 1 to black
    norm = colors.BoundaryNorm(bounds, cmap.N)
    plt.imshow(nonogram_matrix, cmap=cmap, norm=norm)
    plt.axis("off")
    if output_file_path:
        plt.savefig(output_file_path, bbox_inches="tight", pad_inches=0)
    plt.show()


if __name__ == "__main__":
    nonogram_data = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [-1, -1, -1, -1, -1, 1, -1, -1, -1, -1, -1],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]

    display_nonogram(nonogram_data, "nonogram_pic.png")
