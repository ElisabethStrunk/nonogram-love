from operator import itemgetter
from itertools import combinations
from typing import List, Dict, Tuple, Optional


LineIdentifier = Tuple[Optional[int], Optional[int]]


class LinePriorityQueue:
    def __init__(self):
        self._priority_dict: Dict = dict()

    def put(self, line_identifier: LineIdentifier, priority: int):
        self._priority_dict.update({line_identifier: priority})

    def get(self):
        line_with_highest_priority = \
            max(self._priority_dict.items(), key=itemgetter(1))[0] if self._priority_dict else None
        if line_with_highest_priority:
            self._priority_dict.pop(line_with_highest_priority)
        return line_with_highest_priority

    def increase_priority(self, line_identifier: LineIdentifier):
        if line_identifier not in self._priority_dict:
            self._priority_dict.update({line_identifier: 1})
        else:
            self._priority_dict[line_identifier] = self._priority_dict[line_identifier] + 1

    def decrease_priority(self, line_identifier: LineIdentifier):
        if line_identifier not in self._priority_dict:
            self._priority_dict.update({line_identifier: 1})
        else:
            self._priority_dict[line_identifier] = max(self._priority_dict[line_identifier] - 1, 1)


class NonogramSolver:
    def __init__(self, row_hints: List[List[int]], column_hints: List[List[int]]):
        self.row_hints: List[List[int]] = row_hints
        self.column_hints: List[List[int]] = column_hints

        self.number_of_rows: int = len(row_hints)
        self.number_of_columns: int = len(column_hints)

        # initialize solution matrix
        self.solution_matrix = [[-1 for _ in range(self.number_of_columns)] for _ in range(self.number_of_rows)]

        # initialize priority queue
        self.line_priority_queue: LinePriorityQueue = LinePriorityQueue()
        self._initialize_priority_queue()

        # prepare possible line solutions
        self.possible_line_solutions: Dict[Tuple, List[List[int]]] = dict()
        self._prepare_possible_line_solutions()

    def solve(self) -> List[List[int]]:
        line_to_process = self.line_priority_queue.get()
        counter = 0
        while line_to_process and not self.solution_reached():
            self._process_line(line_identifier=line_to_process)
            line_to_process = self.line_priority_queue.get()
            counter += 1
            if counter % 50 == 0:
                print(counter)
                for row in self.solution_matrix:
                    print(row)
                print("\n")
        return self.solution_matrix

    def solution_reached(self) -> bool:
        for row in self.solution_matrix:
            if -1 in row:
                return False
        return True

    def _process_line(self, line_identifier: LineIdentifier):
        line_type: str = "row" if line_identifier[0] is not None else "column"
        index: int = line_identifier[0] if line_type == "row" else line_identifier[1]
        constraints: List[int] = self._get_line_in_solution_matrix(line_identifier=line_identifier)

        possible_line_solutions = self.possible_line_solutions[line_identifier]
        valid_line_solutions = self._remove_invalid_solutions(solutions=possible_line_solutions,
                                                              constraints=constraints)
        if not valid_line_solutions:
            raise Exception(f"Inconsistent nonogram hints detected: {line_type} {index} has no valid solution!")
        self.possible_line_solutions[line_identifier] = valid_line_solutions
        self._add_solved_squares(line_identifier, valid_line_solutions)

    def _get_line_in_solution_matrix(self, line_identifier: LineIdentifier) -> List[int]:
        line_type: str = "row" if line_identifier[0] is not None else "column"
        index: int = line_identifier[0] if line_type == "row" else line_identifier[1]
        return self.solution_matrix[index] if line_type == "row" else [row[index] for row in self.solution_matrix]

    def _initialize_priority_queue(self):
        for row_index, hint_values in enumerate(self.row_hints):
            priority = sum(hint_values) + len(hint_values) - 1 if hint_values else self.number_of_columns
            self.line_priority_queue.put(line_identifier=(row_index, None), priority=priority)
        for column_index, hint_values in enumerate(self.column_hints):
            priority = sum(hint_values) + len(hint_values) - 1 if hint_values else self.number_of_columns
            self.line_priority_queue.put(line_identifier=(None, column_index), priority=priority)

    def _prepare_possible_line_solutions(self):
        for row_index, hint_values in enumerate(self.row_hints):
            possible_line_solution: List[List[int]] = \
                self._possible_line_solutions(line_hints=hint_values, line_length=self.number_of_columns)
            self.possible_line_solutions.update({(row_index, None): possible_line_solution})
        for column_index, hint_values in enumerate(self.column_hints):
            possible_line_solution: List[List[int]] = \
                self._possible_line_solutions(line_hints=hint_values, line_length=self.number_of_rows)
            self.possible_line_solutions.update({(None, column_index): possible_line_solution})

    @classmethod
    def _possible_line_solutions(cls, line_hints: List[int], line_length: int) -> List[List[int]]:
        # *** determine all possible solutions ***
        number_of_groups: int = len(line_hints)
        number_of_black_squares: int = sum(line_hints)
        number_of_separator_white_squares: int = number_of_groups - 1 if number_of_groups > 0 else 0
        number_of_other_white_squares: int = line_length - number_of_black_squares - number_of_separator_white_squares

        possible_line_solutions = []
        number_of_elements_in_simplified_solution: int = number_of_groups + number_of_other_white_squares
        for combination in combinations(range(number_of_elements_in_simplified_solution), number_of_groups):
            simplified_solution = [1 if index in combination else 0
                                   for index, _ in enumerate(range(number_of_elements_in_simplified_solution))]
            if number_of_groups > 1:
                # insert separator white squares
                number_of_inserted_zeroes = 0
                for index in combination[:-1]:
                    simplified_solution.insert(index + number_of_inserted_zeroes + 1, 0)
                    number_of_inserted_zeroes += 1

            # assemble solution using group sizes given by the line hints
            hint_index = 0
            solution = list()
            for square in simplified_solution:
                if square == 1:
                    number_of_ones_to_append = line_hints[hint_index]
                    solution.extend([1] * number_of_ones_to_append)
                    hint_index += 1
                else:  # square == 0
                    solution.append(0)

            possible_line_solutions.append(solution)
        return possible_line_solutions

    @classmethod
    def _remove_invalid_solutions(cls, solutions: List[List[int]], constraints: List[int]) -> List[List[int]]:
        # *** remove solutions that don't meet the criteria in the solution matrix ***
        valid_solutions: List[List[int]] = list()
        for solution in solutions:
            solution_valid = True
            for index, value in enumerate(solution):
                if constraints[index] != -1 and value != constraints[index]:
                    solution_valid = False
                    break
            if solution_valid:
                valid_solutions.append(solution)
        return valid_solutions

    def _add_solved_squares(self, line_identifier: LineIdentifier, valid_solutions: List[List[int]]):
        if len(valid_solutions) == 1:
            # only remaining valid solution -> line is solved
            solution = valid_solutions[0]
            for square_index, square_value in enumerate(solution):
                self._update_solution_matrix_and_priority_queue(line_identifier, square_index, square_value)
        else:
            number_of_squares = len(valid_solutions[0])
            for square_index in range(number_of_squares):
                if len(set([s[square_index] for s in valid_solutions])) == 1:
                    # square is solved
                    square_value = valid_solutions[0][square_index]
                    self._update_solution_matrix_and_priority_queue(line_identifier, square_index, square_value)

    def _update_solution_matrix_and_priority_queue(self,
                                                   line_identifier: LineIdentifier,
                                                   square_index: int,
                                                   square_value: int):
        line_type: str = "row" if line_identifier[0] is not None else "column"
        index: int = line_identifier[0] if line_type == "row" else line_identifier[1]
        if line_type == "row":
            previous_value = self.solution_matrix[index][square_index]
            if previous_value == -1:
                self.solution_matrix[index][square_index] = square_value
                self.line_priority_queue.increase_priority((None, square_index))
            elif previous_value != square_value:
                raise Exception(f"Inconsistent nonogram hints detected: encountered contradiction in "
                                f"square {index, square_index}!")
        else:  # line_type == "column":
            previous_value = self.solution_matrix[square_index][index]
            if previous_value == -1:
                self.solution_matrix[square_index][index] = square_value
                self.line_priority_queue.increase_priority((square_index, None))
            elif previous_value != square_value:
                raise Exception(f"Inconsistent nonogram hints detected: encountered contradiction in "
                                f"square {square_index, index}!")


if __name__ == "__main__":
    from nonogram.nonogram_maker import generate_nonogram_hints
    from nonogram.nonogram_printer import display_nonogram
    from picture_data import tower_solvable as nonogram_data

    nonogram_hints = generate_nonogram_hints(nonogram_data)

    solver = NonogramSolver(column_hints=nonogram_hints["column_hints"], row_hints=nonogram_hints["row_hints"])

    solution_matrix = solver.solve()
    print("SOLUTION")
    for row in solution_matrix:
        print(row)
    display_nonogram(solution_matrix)
