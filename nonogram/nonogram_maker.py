from typing import Dict, List
from nonogram.config import WHITE


def generate_nonogram_hints(nonogram_data) -> Dict[str, List[List[int]]]:
    """
    Generates nonogram line hints from the specified nonogram picture data.
    Example:
    Pixel picture of a dog
    ------
     X
      XXX
      x x
    ------
    The nonogram_data must have a WHITE for every blank and a BLACK for each X.
    This data fed to the function generate_nonogram_hints will result in the following output:
    {
        "row_hints": [[1], [3], [1, 1]]
        "column_hints": [[1], [2], [1], [2]]
    }

    :param nonogram_data:   nonogram picture represented by a list of rows, each row being a list of BLACK and WHITE
                            values as defined in the config file
    :return:                nonogram line hints represented by a dictionary with keys 'row_hints' and 'column_hints',
                            the values ba list each, containing the line hints as lists of integers
    """
    w = str(WHITE)

    nonogram_hints: Dict[str, List[List[int]]] = {
        "row_hints": [],
        "column_hints": []
    }

    # numbers for row hints
    for row in nonogram_data:
        row_string: str = "".join([str(x) for x in row])
        black_clusters = [cluster for cluster in row_string.split(w) if cluster]
        nonogram_hints["row_hints"].append([len(cluster) for cluster in black_clusters])

    # numbers for column hints
    for column_index in range(len(nonogram_data[0])):
        column_string: str = "".join([str(row[column_index]) for row in nonogram_data])
        black_clusters = [cluster for cluster in column_string.split(w) if cluster]
        nonogram_hints["column_hints"].append([len(cluster) for cluster in black_clusters])

    return nonogram_hints


if __name__ == "__main__":
    from picture_data import tower_solvable as nonogram_data

    print("Line hints for nonogram: ")
    print(generate_nonogram_hints(nonogram_data))
