from nonogram.config import BLACK, WHITE

b = BLACK
w = WHITE

# heart - unsolvable
# -------------------
#
#   XX   XX
#  X  X X  X
# X    X    X
# X         X
#  X       X
#   X     X
#    X   X
#     X X
#      X
#
# -------------------
heart_unsolvable = [
    [w, w, w, w, w, w, w, w, w, w, w],
    [w, w, b, b, w, w, w, b, b, w, w],
    [w, b, w, w, b, w, b, w, w, b, w],
    [b, w, w, w, w, b, w, w, w, w, b],
    [b, w, w, w, w, w, w, w, w, w, b],
    [w, b, w, w, w, w, w, w, w, b, w],
    [w, w, b, w, w, w, w, w, b, w, w],
    [w, w, w, b, w, w, w, b, w, w, w],
    [w, w, w, w, b, w, b, w, w, w, w],
    [w, w, w, w, w, b, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w],
]

# tower - solvable
# -------------------
# X X X
# XXXXX
#  XXX
#  X X
#  XXX
# -------------------
tower_solvable = [
    [b, w, b, w, b],
    [b, b, b, b, b],
    [w, b, b, b, w],
    [w, b, w, b, w],
    [w, b, b, b, w]
]

# lovers - solvable
# -------------------
# XXXXX
# XXXXXX
# XXXX X    XXXX
# XXX  XX  XXXXXX
# XXX X X XX  XXX
# XX    X X X XXX
# XXX  XX  X   XX
# XXX   X  X   XX
# XX XXX    XXXXX
#   XXXXXXX X XXX
#  XX  XX XX  XX
#  XX   XX   XXX
#  XXX   XXXXX XX
#  XXXX         X
#  XXXXXXXXXXXXXX
# -------------------
lovers_solvable = [
    [b, b, b, b, b, w, w, w, w, w, w, w, w, w, w],
    [b, b, b, b, b, b, w, w, w, w, w, w, w, w, w],
    [b, b, b, b, w, b, w, w, w, w, b, b, b, b, w],
    [b, b, b, w, w, b, b, w, w, b, b, b, b, b, b],
    [b, b, b, w, b, w, b, w, b, b, w, w, b, b, b],
    [b, b, w, w, w, w, b, w, b, w, b, w, b, b, b],
    [b, b, b, w, w, b, b, w, w, b, w, w, w, b, b],
    [b, b, b, w, w, w, b, w, w, b, w, w, w, b, b],
    [b, b, w, b, b, b, w, w, w, w, b, b, b, b, b],
    [w, w, b, b, b, b, b, b, b, w, b, w, b, b, b],
    [w, b, b, w, w, b, b, w, b, b, w, w, b, b, w],
    [w, b, b, w, w, w, b, b, w, w, w, b, b, b, w],
    [w, b, b, b, w, w, w, b, b, b, b, b, w, b, b],
    [w, b, b, b, b, w, w, w, w, w, w, w, w, w, b],
    [w, b, b, b, b, b, b, b, b, b, b, b, b, b, b]
]

# pineapple - unsolvable
# ---------------------------------
#                      X
#                      X     X
#                     XX    XX
#                     XXX  XX
#                     XXX XXX
#                    XXXXXXX  XX
#              XXXXXXXXXXXXXXXXX
#            XXXXXXXXXXXXXXXXXX
#          XXXXXXX XXXXXXXXXXX
#        XXXXX XXX XXX XXXXXX
#       XXXXXX XXX XXX XXXXXXXX
#      XXX               XXXXXXX
#     XXX  XX  XX  XX  XXXX
#    XXXXX XXX XXX XXX XXXX
#   XXXXXX XXX XXX XXX XXXX
#   XX                    X
#  XX  XX  XX  XX  XX  XXXX
#  XXX XXX XXX XXX XXX XXX
# XXXX XXX XXX XXX XXX XXX
# XX                    X
# XXX  XX  XX  XX  XX  XX
# XXXX XXX XXX XXX XXX X
# XXXX XXX XXX XXX XXXXX
# XX                 XX
# XXXX XX  XX  XX  XXX
#  XXX XXX XXX XXX XXX
#   XXXXXX XXX XXXXXX
#    XXX         XXX
#    XXXXX XX  XXXX
#      XXXXXXXXXX
# ---------------------------------
pinapple_unsolvable = [
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, w, b, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w, w, w, b, b, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, w, w, b, b, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, w, b, b, b, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, w, b, b],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b],
    [w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w],
    [w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, w, b, b, b, b, b, b, b, b, b, b, b, w, w],
    [w, w, w, w, w, w, w, b, b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, b, b, b, w, w, w],
    [w, w, w, w, w, w, b, b, b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, b, b, b, b, b, w],
    [w, w, w, w, w, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b],
    [w, w, w, w, b, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, b, b, w, w, w, w, w],
    [w, w, w, b, b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, b, w, w, w, w, w],
    [w, w, b, b, b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, b, w, w, w, w, w],
    [w, w, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, w],
    [w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, b, b, w, w, w, w, w],
    [w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, w, w, w, w, w],
    [b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, w, w, w, w, w],
    [b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, w, w, w, w, w, w, w],
    [b, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, w, w, w, w, w, w, w],
    [b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, w, w, w, w, w, w, w, w],
    [b, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, b, b, w, w, w, w, w, w, w, w],
    [b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w, w, w, w, w, w, w, w],
    [b, b, b, b, w, b, b, w, w, b, b, w, w, b, b, w, w, b, b, b, w, w, w, w, w, w, w, w, w, w],
    [w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, b, b, b, w, w, w, w, w, w, w, w, w, w],
    [w, w, b, b, b, b, b, b, w, b, b, b, w, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, b, b, b, w, w, w, w, w, w, w, w, w, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, b, b, b, b, b, w, b, b, w, w, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w]
]

# car - solvable
# ----------------------------------------------
#                   XXXXXXXXXXX
#                  XXXXX  X XXXXXX
#                 XXXXX   X XXXXXXXX
#                XXX     XX      XXXXX
#               XXX X    XX       XXXXX
#           XXXXX X X   XXXX      XXXXXX
#       XXXXXXXXX XXXXXXXXXXXXXXXXXXXXXX
#     XXXXXXXXXXXX XXXX  X XXXXXXX     XXX
#    XXXXXXX XXXXX XXXXXXX XXXXX  XXXXXXXXX
#   XXX    XX XXXX XXXXXXX XXXX XXX    XXXX
#   XX XXXX XX XXXXXXXXXXX XXXX XX XXXX XXXX
# X X XX  XX XX XXXXXXXXXX XXXX X XX  XX XX X X
# XXXXX XX XX X XXXXXXXXXX XXXXXX X XX XX XXXXX
#  X X X  X X X XXXXXXXXXX XXXXXXX X  X X XXXX
#    X X  X X XXXXXXXXXXXXXXXXXXXX X  X X
#    XX XX XX                    XX XX XX
#     XX  XX                      XX  XX
#      XXXX                        XXXX
# ----------------------------------------------
car_solvable = [
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, w, w, b, w, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, w, w, w, b, w, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, w, w, w, w, w, b, b, w, w, w, w, w, w, b, b, b, b, b, w, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, w, b, w, w, w, w, b, b, w, w, w, w, w, w, w, b, b, b, b, b, w, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, w, w, w, w, b, b, b, b, b, w, b, w, b, w, w, w, b, b, b, b, w, w, w, w, w, w, b, b, b, b, b, b, w, w, w, w, w, w, w],
    [w, w, w, w, w, w, b, b, b, b, b, b, b, b, b, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w, w, w, w, w, w, w],
    [w, w, w, w, b, b, b, b, b, b, b, b, b, b, b, b, w, b, b, b, b, w, w, b, w, b, b, b, b, b, b, b, w, w, w, w, w, b, b, b, w, w, w, w, w],
    [w, w, w, b, b, b, b, b, b, b, w, b, b, b, b, b, w, b, b, b, b, b, b, b, w, b, b, b, b, b, w, w, b, b, b, b, b, b, b, b, b, w, w, w, w],
    [w, w, b, b, b, w, w, w, w, b, b, w, b, b, b, b, w, b, b, b, b, b, b, b, w, b, b, b, b, w, b, b, b, w, w, w, w, b, b, b, b, w, w, w, w],
    [w, w, b, b, w, b, b, b, b, w, b, b, w, b, b, b, b, b, b, b, b, b, b, b, w, b, b, b, b, w, b, b, w, b, b, b, b, w, b, b, b, b, w, w, w],
    [b, w, b, w, b, b, w, w, b, b, w, b, b, w, b, b, b, b, b, b, b, b, b, b, w, b, b, b, b, w, b, w, b, b, w, w, b, b, w, b, b, w, b, w, b],
    [b, b, b, b, b, w, b, b, w, b, b, w, b, w, b, b, b, b, b, b, b, b, b, b, w, b, b, b, b, b, b, w, b, w, b, b, w, b, b, w, b, b, b, b, b],
    [w, b, w, b, w, b, w, w, b, w, b, w, b, w, b, b, b, b, b, b, b, b, b, b, w, b, b, b, b, b, b, b, w, b, w, w, b, w, b, w, b, b, b, b, w],
    [w, w, w, b, w, b, w, w, b, w, b, w, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, b, w, b, w, w, b, w, b, w, w, w, w, w, w],
    [w, w, w, b, b, w, b, b, w, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, b, b, w, b, b, w, w, w, w, w, w],
    [w, w, w, w, b, b, w, w, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, w, w, b, b, w, w, w, w, w, w, w],
    [w, w, w, w, w, b, b, b, b, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, w, b, b, b, b, w, w, w, w, w, w, w, w]
]
