import unittest
from unittest.mock import patch

from nonogram.nonogram_solver import LinePriorityQueue, NonogramSolver


class TestLinePriorityQueue(unittest.TestCase):
    def test_put_happy(self):
        # test inputs
        test_line_identifier_row = (0, None)
        test_line_identifier_column = (None, 2)
        test_priority_row = 5
        test_priority_column = 3
        # run test
        test_priority_queue = LinePriorityQueue()
        test_priority_queue.put(line_identifier=test_line_identifier_row, priority=test_priority_row)
        test_priority_queue.put(line_identifier=test_line_identifier_column, priority=test_priority_column)
        queue_dict = test_priority_queue._priority_dict
        try:
            output_priority_row = queue_dict[test_line_identifier_row]
            output_priority_column = queue_dict[test_line_identifier_column]
        except KeyError:
            raise AssertionError("Expected elements not found in priority dictionary!")
        self.assertEqual(output_priority_row, test_priority_row)
        self.assertEqual(output_priority_column, test_priority_column)

    def test_get_happy(self):
        # test inputs
        test_line_identifier_row = (0, None)
        test_line_identifier_column = (None, 2)
        test_priority_row = 5
        test_priority_column = 3
        # expected values
        expected_line_identifier = test_line_identifier_row  # 5 > 3
        # test setup
        test_priority_queue = LinePriorityQueue()
        test_priority_queue._priority_dict = {
            test_line_identifier_row: test_priority_row,
            test_line_identifier_column: test_priority_column
        }
        # run test
        output_line_identifier = test_priority_queue.get()
        self.assertEqual(output_line_identifier, expected_line_identifier)

    def test_increase_priority_happy(self):
        # test inputs
        test_line_identifier_row = (0, None)
        test_line_identifier_column = (None, 2)
        test_priority_row = 5
        test_priority_column = 3
        # expected values
        expected_priority_row = 6
        expected_priority_column = 3
        # test setup
        test_priority_queue = LinePriorityQueue()
        test_priority_queue._priority_dict = {
            test_line_identifier_row: test_priority_row,
            test_line_identifier_column: test_priority_column
        }
        # run test
        test_priority_queue.increase_priority(line_identifier=test_line_identifier_row)
        queue_dict = test_priority_queue._priority_dict
        try:
            output_priority_row = queue_dict[test_line_identifier_row]
            output_priority_column = queue_dict[test_line_identifier_column]
        except KeyError:
            raise AssertionError("Expected elements not found in priority dictionary!")
        self.assertEqual(output_priority_row, expected_priority_row)
        self.assertEqual(output_priority_column, expected_priority_column)

    def test_decrease_priority_happy(self):
        # test inputs
        test_line_identifier_row = (0, None)
        test_line_identifier_column = (None, 2)
        test_priority_row = 5
        test_priority_column = 3
        # expected values
        expected_priority_row = 5
        expected_priority_column = 2
        # test setup
        test_priority_queue = LinePriorityQueue()
        test_priority_queue._priority_dict = {
            test_line_identifier_row: test_priority_row,
            test_line_identifier_column: test_priority_column
        }
        # run test
        test_priority_queue.decrease_priority(line_identifier=test_line_identifier_column)
        queue_dict = test_priority_queue._priority_dict
        try:
            output_priority_row = queue_dict[test_line_identifier_row]
            output_priority_column = queue_dict[test_line_identifier_column]
        except KeyError:
            raise AssertionError("Expected elements not found in priority dictionary!")
        self.assertEqual(output_priority_row, expected_priority_row)
        self.assertEqual(output_priority_column, expected_priority_column)


class TestNonogramSolver(unittest.TestCase):
    def setUp(self):
        # the Tower nonogram is used for testing
        test_row_hints = [[1, 1, 1], [5], [3], [1, 1], [3]]
        test_column_hints = [[2], [4], [3, 1], [4], [2]]
        self.nonogram_solver = NonogramSolver(row_hints=test_row_hints, column_hints=test_column_hints)

    def test_initialize_priority_queue_happy(self):
        # expected values
        expected_priority_queue_dict = {
            (0, None): 5,
            (1, None): 5,
            (2, None): 3,
            (3, None): 3,
            (4, None): 3,
            (None, 0): 2,
            (None, 1): 4,
            (None, 2): 5,
            (None, 3): 4,
            (None, 4): 2
        }
        # run test
        output_priority_queue_dict = self.nonogram_solver.line_priority_queue._priority_dict
        self.assertEqual(output_priority_queue_dict, expected_priority_queue_dict)

    def test_solve_happy(self):
        """
        Integration test
        """
        # test inputs
        # expected values
        expected_solution_matrix = [
            [1, 0, 1, 0, 1],
            [1, 1, 1, 1, 1],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0]
        ]
        # run test
        output_solution_matrix = self.nonogram_solver.solve()
        self.assertEqual(output_solution_matrix, expected_solution_matrix)

    def test_solution_reached_happy(self):
        # test inputs
        test_solution_matrix_unsolved = [
            [1, 0, 1, 0, 1],
            [1, 1, -1, 1, 1],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, -1, 1, 1, 0]
        ]
        test_solution_matrix_solved = [
            [1, 0, 1, 0, 1],
            [1, 1, 1, 1, 1],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0]
        ]
        # expected values
        expected_flag_for_unsolved = False
        expected_flag_for_solved = True
        # run test
        self.nonogram_solver.solution_matrix = test_solution_matrix_unsolved
        output_flag_unsolved = self.nonogram_solver.solution_reached()
        self.assertEqual(output_flag_unsolved, expected_flag_for_unsolved)

        self.nonogram_solver.solution_matrix = test_solution_matrix_solved
        output_flag_solved = self.nonogram_solver.solution_reached()
        self.assertEqual(output_flag_solved, expected_flag_for_solved)

    @patch("nonogram.nonogram_solver.NonogramSolver._get_line_in_solution_matrix")
    @patch("nonogram.nonogram_solver.NonogramSolver._remove_invalid_solutions")
    @patch("nonogram.nonogram_solver.NonogramSolver._add_solved_squares")
    def test__process_line_happy(self,
                                 mocked_add_solved_squares,
                                 mocked_remove_invalid_solutions,
                                 mocked_get_line_in_solution_matrix):
        # test inputs
        test_line_identifier = (0, None)  # first row

        # mocked values
        mocked_line_in_solution_matrix = [-1, -1, -1, -1, -1]
        mocked_possible_solutions = [[1, 0, 1, 0, 1]]
        mocked_valid_line_solutions = mocked_possible_solutions
        mocked_get_line_in_solution_matrix.side_effect = [mocked_line_in_solution_matrix]
        mocked_remove_invalid_solutions.side_effect = [mocked_valid_line_solutions]

        # expected values
        expected_line_identifier = test_line_identifier
        expected_possible_line_solutions = mocked_possible_solutions
        expected_constraints = mocked_line_in_solution_matrix
        expected_valid_line_solutions = mocked_valid_line_solutions

        # run test
        self.nonogram_solver._process_line(test_line_identifier)
        mocked_get_line_in_solution_matrix.assert_called_with(line_identifier=expected_line_identifier)
        mocked_remove_invalid_solutions.assert_called_with(solutions=expected_possible_line_solutions,
                                                           constraints=expected_constraints)
        mocked_add_solved_squares.assert_called_with(expected_line_identifier, expected_valid_line_solutions)

    def test__get_line_in_solution_matrix_happy(self):
        # test inputs
        test_line_identifier = (0, None)  # first row
        # mocked values
        mocked_solution_matrix_solved = [
            [1, 0, 1, 0, 1],
            [1, 1, 1, 1, 1],
            [0, 1, 1, 1, 0],
            [0, 1, 0, 1, 0],
            [0, 1, 1, 1, 0]
        ]
        # expected values
        expected_line_in_solution_matrix_unsolved = [-1, -1, -1, -1, -1]
        expected_line_in_solution_matrix_solved = [1, 0, 1, 0, 1]
        # run test
        output_line_in_solution_matrix_unsolved = \
            self.nonogram_solver._get_line_in_solution_matrix(test_line_identifier)
        self.assertEqual(output_line_in_solution_matrix_unsolved, expected_line_in_solution_matrix_unsolved)

        self.nonogram_solver.solution_matrix = mocked_solution_matrix_solved
        output_line_in_solution_matrix_solved = self.nonogram_solver._get_line_in_solution_matrix(test_line_identifier)
        self.assertEqual(output_line_in_solution_matrix_solved, expected_line_in_solution_matrix_solved)

    def test__possible_line_solutions_happy(self):
        # test inputs
        test_line_hints_1 = [1, 1, 1]
        test_line_hints_2 = [1, 2]
        test_line_length = 5
        # expected values
        expected_line_solutions_1 = [[1, 0, 1, 0, 1]]
        expected_line_solutions_2 = [[1, 0, 1, 1, 0], [1, 0, 0, 1, 1], [0, 1, 0, 1, 1]]
        # run test
        output_line_solutions_1 = self.nonogram_solver._possible_line_solutions(line_hints=test_line_hints_1,
                                                                                line_length=test_line_length)
        output_line_solutions_2 = self.nonogram_solver._possible_line_solutions(line_hints=test_line_hints_2,
                                                                                line_length=test_line_length)
        self.assertEqual(output_line_solutions_1, expected_line_solutions_1)
        self.assertEqual(output_line_solutions_2, expected_line_solutions_2)

    def test__remove_invalid_solutions_happy(self):
        # test inputs
        test_solutions = [[1, 0, 1, 1, 0], [1, 0, 0, 1, 1], [0, 1, 0, 1, 1]]
        test_constraints = [1, -1, -1, -1, 0]
        # expected values
        expected_valid_solutions = [[1, 0, 1, 1, 0]]
        # run test
        output_valid_solutions = self.nonogram_solver._remove_invalid_solutions(solutions=test_solutions,
                                                                                constraints=test_constraints)
        self.assertEqual(output_valid_solutions, expected_valid_solutions)

    def test__add_solved_squares_happy(self):
        """
        Integration test
        (_add_solved_squares  and  _update_solution_matrix_and_priority_queue)
        """
        # ***** Test Case 1: Only one valid solution left *****
        # test inputs
        test_line_identifier_1 = (0, None)  # first row
        test_valid_solutions_1 = [[1, 0, 1, 1, 0]]
        # expected values
        expected_solution_matrix_1 = [
            [1, 0, 1, 1, 0],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ]
        # run test
        self.nonogram_solver._add_solved_squares(line_identifier=test_line_identifier_1,
                                                 valid_solutions=test_valid_solutions_1)
        output_solution_matrix_1 = self.nonogram_solver.solution_matrix
        self.assertEqual(output_solution_matrix_1, expected_solution_matrix_1)

        # ***** Test Case 2: Several valid solutions left *****
        # test inputs
        test_line_identifier_2 = (2, None)  # 3rd row
        test_valid_solutions_2 = [[1, 0, 1, 1, 0], [1, 0, 0, 1, 1]]
        # expected values
        expected_solution_matrix_2 = [
            [1, 0, 1, 1, 0],
            [-1, -1, -1, -1, -1],
            [1, 0, -1, 1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ]
        # run test
        self.nonogram_solver._add_solved_squares(line_identifier=test_line_identifier_2,
                                                 valid_solutions=test_valid_solutions_2)
        output_solution_matrix_2 = self.nonogram_solver.solution_matrix
        self.assertEqual(output_solution_matrix_2, expected_solution_matrix_2)

    @patch("nonogram.nonogram_solver.LinePriorityQueue.increase_priority")
    def test__update_solution_matrix_and_priority_queue_happy(self, mocked_increase_priority):
        # test inputs
        test_line_identifier = (None, 0)  # first column
        test_square_index_1 = 0
        test_square_index_2 = 2
        test_square_value_black = 1
        test_square_value_white = 0

        # mocked values
        mocked_solution_matrix = [
            [1, 0, 1, 1, 0],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ]

        # expected values
        expected_solution_matrix = [
            [1, 0, 1, 1, 0],
            [-1, -1, -1, -1, -1],
            [0, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1],
            [-1, -1, -1, -1, -1]
        ]
        expected_error_message = "Inconsistent nonogram hints detected: encountered contradiction in square (0, 0)!"

        # run test
        self.nonogram_solver.solution_matrix = mocked_solution_matrix
        self.nonogram_solver._update_solution_matrix_and_priority_queue(line_identifier=test_line_identifier,
                                                                        square_index=test_square_index_1,
                                                                        square_value=test_square_value_black)
        self.nonogram_solver._update_solution_matrix_and_priority_queue(line_identifier=test_line_identifier,
                                                                        square_index=test_square_index_2,
                                                                        square_value=test_square_value_white)
        with self.assertRaises(Exception) as cm:
            self.nonogram_solver._update_solution_matrix_and_priority_queue(line_identifier=test_line_identifier,
                                                                            square_index=test_square_index_1,
                                                                            square_value=test_square_value_white)
        raised_exception = cm.exception
        self.assertEqual(str(raised_exception), expected_error_message)
