import unittest


class TestNonogramMaker(unittest.TestCase):
    def test_generate_nonogram_hints_happy(self):
        """
        'Happy' test case for code.nonogram_maker.generate_nonogram_hints_happy
        """
        # test inputs
        from nonogram.config import BLACK, WHITE
        b = BLACK
        w = WHITE
        test_nonogram_data = [
            [w, w, w, w, w, w, w, w, w, w, w],
            [w, w, b, b, w, w, w, b, b, w, w],
            [w, b, w, w, b, w, b, w, w, b, w],
            [b, w, w, w, w, b, w, w, w, w, b],
            [b, w, w, w, w, w, w, w, w, w, b],
            [w, b, w, w, w, w, w, w, w, b, w],
            [w, w, b, w, w, w, w, w, b, w, w],
            [w, w, w, b, w, w, w, b, w, w, w],
            [w, w, w, w, b, w, b, w, w, w, w],
            [w, w, w, w, w, b, w, w, w, w, w],
            [w, w, w, w, w, w, w, w, w, w, w],
        ]

        # expected values
        expected_nonogram_hints = {
            "row_hints": [[], [2, 2], [1, 1, 1, 1], [1, 1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1], []],
            "column_hints": [[2], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [1, 1], [2]]
        }

        # run test
        from nonogram.nonogram_maker import generate_nonogram_hints
        output_nonogram_hints = generate_nonogram_hints(test_nonogram_data)
        self.assertEqual(output_nonogram_hints, expected_nonogram_hints)
